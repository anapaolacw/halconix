import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import os
import sys
import subprocess
import time
from wifi import Cell, Scheme	
import urllib2	
from urllib2 import urlopen
from threading import Timer

cmd = os.listdir('/sys/class/net/')
for c in cmd:
    if c.startswith("w"):
        interface = c

class MainWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Configuracion de Redes")
        self.table = Gtk.Table(3, 2, True)
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)

        if modoAvion == True:
            Gtk.Window.set_title(self, "Error. Desactive el modo avion")
            self.labelError = Gtk.Label("                      No hay redes disponibles                    ")
            self.grid.attach(self.labelError, 0, 0, 1, 1)
            self.add(self.grid)
        else:
            #Get networks information
            cmd = os.listdir('/sys/class/net/')
            for c in cmd:
                if c.startswith("w"):
                    self.interface = c
            cells = Cell.all(self.interface)
            if len(cells) <  1:
                Gtk.Window.set_title(self, "No hay redes disponibles")
                self.labelError = Gtk.Label("No hay redes disponibles")
                self.grid.attach(self.labelError, 0, 0, 1, 1)
                self.add(self.grid)
            else:
                self.sorted_cells = sorted(cells, key= lambda h: (h.quality), reverse=True)
    
                for cell in self.sorted_cells:
                    n1 = int((str(cell.quality)).split("/")[0])
                    n2 = int((str(cell.quality)).split("/")[1])
                    cell.quality = ((n1*100)/n2)
                posicionX = 0
                posicionY = 0
                for network in self.sorted_cells:
                    if network.encrypted:
                        seguridad = "Con clave de acceso."
                    else:
                        seguridad = "Sin clave de acceso."
                    texto = network.ssid + ", Intensidad: " + str(network.quality) +"%, " + seguridad
                    
                    self.buttonTemp = Gtk.Button(label= texto, name = network.ssid)
                    self.buttonTemp.connect("clicked", self.connect_clicked)
                    self.grid.attach(self.buttonTemp, posicionX, posicionY, 1, 1)
                    posicionY = posicionY + 1 
                self.add(self.grid)
    
        
    def connect_clicked(self, button):
        print("Se quiere conectar a " +button.get_name())
        for net in self.sorted_cells:
            if net.ssid == button.get_name():
                cell = net
        if cell:
            if cell.encrypted:
                print "Encriptada"
                #verificamos si la red ya estaba guardada.....
                findedPassword = self.findOnSaved(cell, True)
                if findedPassword != False:
                    print "Ya estaba la contrasena"
                    #Ya estaba guardada la clave
                    self.replaceActualNetwork(cell, findedPassword)
                    if self.activateNetwork(cell, findedPassword, False, True) == False:
                        #Error activandola
                        wPass = self.askPassword()
                        if wPass: 
                            ePass = self.encryptPassword(cell, wPass)
                            self.replacePassword(cell, ePass)
                            self.replaceActualNetwork(cell, ePass)
                            self.activateNetwork(cell, ePass, True, True)
                else:
                    print "Pidiendo contrasena"
                    wPass = self.askPassword()
                    if wPass: 
                        ePass = self.encryptPassword(cell, wPass)
                        self.replaceActualNetwork(cell, ePass)
                        self.addSavedNetworks(cell, ePass)
                        self.activateNetwork(cell, ePass, True, True)
            else:
                #Red no encriptada
                if self.findOnSaved(cell, False) == False:
                    self.addSavedNetworks(cell, None)
                self.replaceActualNetwork(cell, None)
                try: 
                    self.activateNetwork(cell, None, True, False)
                    self.sendMessage(0)
                except:
                    print "Error activando red sin clave"
        else:
            print "error de cell"
        return cell

    def sendMessage(self, n):
        if n == 0:
            print "Mensaje exito"
            md = Gtk.MessageDialog (self, Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT, Gtk.MessageType.OTHER, Gtk.ButtonsType.CLOSE, "Conexion establecida")

        elif n == 1:
            print "Mensaje error"
            md = Gtk.MessageDialog (self, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.CLOSE, "Error de conexion ")
        
        #md.destroy();
        if (n == 0 or n == 1):
            response = md.run()
            if response == Gtk.ResponseType.CLOSE:
                md.destroy()
                Gtk.main_quit()
                #sys.exit()

    def encryptPassword(self, cell, password):
        s = Scheme.for_cell(self.interface, cell.ssid, cell, password)
        o = s.options
        print "Encrypted: " +o["wpa-psk"]
        return o["wpa-psk"]
        

    def activateNetwork(self, cell, password, final, needTest):
        try:
            #ahora activaremos esa red
            #if password != None:
            try:
                r = subprocess.check_output(['ifdown', self.interface])
                print "If down"
                r = subprocess.check_output(['/etc/init.d/networking', 'restart'])
                print "start"
                r = subprocess.check_output(['ifup', self.interface])
                print "ifup"
            except Exception as e:
                return False
                print "Error levantando la red"
                print e
            

            #else:
             #   print "No tiene contrasena"
            if needTest == True:
                if self.testConnection() == True:
                    print "HAY INTERNET"
                    self.sendMessage(0)
                    return True
                else:
                    e = ("NO HAY INTERNET") 
                    print e
                    self.sendMessage(1) 
                    return False
            return True
        except Exception as e: 
            print "Error " + str(e)
            self.deleteNetwork(cell, needTest)
            if final == True:
                self.sendMessage(1) 
            else:
                print("Exception False") 
                return False

    def testConnection(self):
        try:
            urllib2.urlopen('http://216.58.192.142', timeout=5)
            return True
        except urllib2.URLError as err: 
            return False

    def replacePassword(self, cell, password):
        print "Replacing password"
        with open('/etc/network/interfaces.d/savedNetworks.txt', 'r') as file:
            data = file.readlines()
            number = 0
            for line in data:
                if cell.ssid in line:
                    netLine = number
                number = number + 1
        data[netLine+1] = password + "\n"
        with open('/etc/network/interfaces.d/savedNetworks.txt', 'w') as file:
            file.writelines(data)  
        print "Replazada"
        return True
        
    def deleteNetwork(self, cell, hasPassword):
        print "deleting " +cell.ssid
        with open('/etc/network/interfaces.d/savedNetworks.txt', 'r') as file:
            data = file.readlines()
            number = 0
            for line in data:
                if cell.ssid in line:
                    netLine = number
                number = number + 1
        del data[netLine] #nombre
        del data[netLine] #espacio
        if hasPassword:
            del data[netLine] #contrasena
        with open('/etc/network/interfaces.d/savedNetworks.txt', 'w') as file:
            file.writelines(data)  
        print "Net removed"

    def findOnSaved(self, cell, encrypted):
        with open('/etc/network/interfaces.d/savedNetworks.txt', 'r') as file:
            data = file.readlines()
            number = 0
            for line in data:
                if cell.ssid in line:
                    netLine = number
                    if encrypted:
                        netPass = data[netLine+1]
                        return netPass
                    else:
                        return True
                number = number + 1
            return False
        
    def replaceActualNetwork(self, cell, password=None):
        data = []
        data.append("\niface wlp5s0 inet dhcp")
        if password:
            data.append("\n    wpa-ssid " +cell.ssid)
            data.append("\n    wpa-psk " +password)
        else:
            data.append("\n    wireless-essid " +cell.ssid)
        data.append("\n    wireless-channel auto")
        print "2 red reemplazada"
        with open('/etc/network/interfaces.d/actualNetwork.txt', 'w') as file:
            file.writelines(data)

    def addSavedNetworks(self, cell, password):
        with open('/etc/network/interfaces.d/savedNetworks.txt', 'r') as file:
            data = file.readlines()
        data.append("\n" +cell.ssid +"\n")
        if password:
            data.append(password +"\n")
        with open('/etc/network/interfaces.d/savedNetworks.txt', 'w') as file:
            file.writelines(data)    
        
        
    def askPassword(self):
        passWindow = Gtk.MessageDialog(self, Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT, Gtk.MessageType.QUESTION, Gtk.ButtonsType.OK_CANCEL)
        passWindow.set_title("Clave de acceso")
        dialogBox = passWindow.get_content_area()

        passEntry = Gtk.Entry()
        passEntry.set_visibility(False)
        passEntry.set_size_request(250, 0)
        # enter key should trigger the default action
        passWindow.set_default_response(Gtk.ResponseType.OK)
        passEntry.set_activates_default(True)

        dialogBox.pack_end(passEntry, False, False, 0)
        passWindow.show_all()
        response = passWindow.run()
        password = passEntry.get_text()
        if (response == Gtk.ResponseType.OK) and (password != ''): 
            print "Contrasena ingresada"
            return password
        elif response == Gtk.ResponseType.CANCEL:
            passWindow.destroy();
        else:
            passWindow.destroy();
            '''error'''
            self.sendMessage(1)
            print "Error al ingresar contrasena"
            return False

modoAvion = False
try: 
    window = MainWindow() 
except Exception as e:
    print "Error de la interfaz"
    if "Network is down" in str(e):
        print "network is down "
        comando = "ifconfig "+interface +" up"
        r = os.system(comando)  
        if r == 65280:
            modoAvion = True
window = MainWindow() 
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()

